import {
  MailOutlined,
  LockOutlined,
  UserOutlined,
  PhoneOutlined,
} from "@ant-design/icons";
import { Input, Form, Button, InputNumber, message } from "antd";
import React from "react";
import { userService } from "../service/userServices";
import { useNavigate } from "react-router-dom";

export default function RegisterPage() {
  let navigate = useNavigate();

  const onFinish = (values) => {
    // console.log("Thanh cong", values);
    userService
      .userRegister(values)
      .then((res) => {
        message.success(res.data.message);
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div
      className="grid justify-center content-center"
      style={{ width: "100%", height: "100vh" }}
    >
      <h1 className="font-thin text-center py-5 text-4xl">
        Sign up - CyberBug
      </h1>
      <div style={{ width: 300, maxWidth: 800, margin: "20px auto" }}>
        <Form onFinish={onFinish} name="basic">
          <Form.Item
            name="email"
            rules={[
              { required: true, message: "Email is requied!" },
              { type: "email", message: "Please input your email address!" },
            ]}
          >
            <Input size="large" prefix={<MailOutlined />} placeholder="Email" />
          </Form.Item>
          <Form.Item
            name="passWord"
            rules={[
              { required: true, message: "Please input your password!" },
              {
                min: 6,
                message:
                  "Password must contain at least 6 characters, please try again!",
              },
            ]}
          >
            <Input.Password
              size="large"
              prefix={<LockOutlined />}
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item
            name="name"
            rules={[{ required: true, message: "Please enter your username" }]}
          >
            <Input
              size="large"
              prefix={<UserOutlined />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="phoneNumber"
            rules={[
              {
                required: true,
                message: "Please input your phone number!",
              },
              {
                pattern: new RegExp("^[0-9]*$"),
                message: "Please enter number only!",
              },
            ]}
          >
            <Input size="large" prefix={<PhoneOutlined />} />
          </Form.Item>
          <Form.Item>
            <Button
              className="bg-blue-700 text-white h-10"
              style={{
                width: 300,
                maxWidth: 800,
              }}
              htmlType="submit"
            >
              Sign up
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
