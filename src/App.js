import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import "./App.css";
import LoginPage from "./LoginPage/LoginPage";
import ProjectManagementPage from "./ProjectManagementPage/ProjectManagementPage";
import MainLayout from "./Layout/MainLayout";
import UserManagementPage from "./Components/UserManagementPage/UserManagementPage";
import RegisterPage from "./RegisterPage/RegisterPage";
import CreateProjectPage from "./ProjectManagementPage/CreateProjectPage";
import ProjectView from "./ProjectManagementPage/ProjectView";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="*" element={<Navigate to={"projectsManagement"} />} />
          <Route path="/" element={<LoginPage />} />
          <Route path="/loginPage" element={<LoginPage />} />
          <Route path="/registerPage" element={<RegisterPage />} />
          <Route
            path="/projectsManagement"
            element={<MainLayout Component={<ProjectManagementPage />} />}
          />
          <Route
            path="/usersManagement"
            element={<MainLayout Component={<UserManagementPage />} />}
          />
          <Route
            path="/createProject"
            element={<MainLayout Component={<CreateProjectPage />} />}
          />
          <Route path="/projectview" element={<ProjectView />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
