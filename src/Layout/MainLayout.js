import React, { useState } from "react";
import HeaderComponent from "../Components/Header/HeaderComponent";
import FooterComponent from "../Components/Footer/FooterComponent";
import Modal from "../Components/Modal/Modal";

export default function MainLayout({ Component }) {
  const [openModal, setOpenModal] = useState(false);

  return (
    <div>
      <HeaderComponent setOpen={setOpenModal} />
      <Modal
        open={openModal}
        onClose={() => {
          setOpenModal(false);
        }}
      />
      {Component}
      <FooterComponent />
    </div>
  );
}
