import { VIEW_ALL_CATEGORY, VIEW_ALL_PROJECTS } from "./const/const";

let initialState = {
  projects: [],
  projectCategory: [],
};

let projectReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case VIEW_ALL_PROJECTS: {
      return {
        ...state,
        projects: payload,
      };
    }
    case VIEW_ALL_CATEGORY: {
      return {
        ...state,
        projectCategory: payload,
      };
    }
    default:
      return state;
  }
};

export default projectReducer;
