import { combineReducers } from "redux";
import projectReducer from "./projectReducer";
import userReducer from "./userReducer";

export const rootReducer = combineReducers({
  projectReducer,
  userReducer,
});
