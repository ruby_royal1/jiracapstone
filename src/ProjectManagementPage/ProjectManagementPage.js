import React, { useEffect, useState } from "react";
import { Layout, Table, theme, Button, message } from "antd";
import { headerProject } from "./utility";
import projectServices from "../service/projectServices";

const ProjectManagementPage = () => {
  const { Content } = Layout;
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  let [projectList, setProjectList] = useState([]);

  const fetchAllProject = () => {
    projectServices
      .getProjects()
      .then((res) => {
        console.log(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    projectServices
      .getProjects()
      .then((res) => {
        let newProjectArr = res.data.content.map((project) => {
          return {
            ...project,
            action: (
              <>
                <Button>Edit</Button>
                <Button
                  onClick={() => {
                    handleDeleteProject(project.id);
                  }}
                >
                  Delete
                </Button>
              </>
            ),
          };
        });
        setProjectList(newProjectArr);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [projectList]);

  let handleDeleteProject = (projectID) => {
    projectServices
      .deleteProject(projectID)
      .then((res) => {
        fetchAllProject();
        message.success("Xoa thanh cong!");
      })
      .catch((err) => {
        projectServices.getProjects();
        message.error(err.response.data.content);
      });
  };

  return (
    <Layout>
      <Content
        className="site-layout"
        style={{
          padding: "0 50px",
        }}
      >
        <div
          className="container"
          style={{
            padding: 24,
            minHeight: 380,
            background: colorBgContainer,
          }}
        >
          <Table dataSource={projectList} columns={headerProject} />
        </div>
      </Content>
    </Layout>
  );
};
export default ProjectManagementPage;
