import React, { useEffect, useState } from "react";
import { Button, Form, Input, Select, message } from "antd";
// import { Editor } from "@tinymce/tinymce-react";
import { useDispatch, useSelector } from "react-redux";
import projectServices from "../service/projectServices";
import { VIEW_ALL_CATEGORY } from "../redux/const/const";
import TextArea from "antd/es/input/TextArea";

const { Option } = Select;

export default function CreateProjectPage() {
  const dispatch = useDispatch();
  let projectCategory = useSelector(
    (state) => state.projectReducer.projectCategory
  );

  useEffect(() => {
    projectServices
      .getAllProjectCategory()
      .then((res) => {
        dispatch({
          type: VIEW_ALL_CATEGORY,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err.response.data.content);
      });
  }, []);

  const onFinish = (values) => {
    console.log(values);
    projectServices
      .createProject(values)
      .then((res) => {
        console.log(res);
        message.success(res.data.message);
        projectServices.getProjects();
      })
      .catch((err) => {
        console.log(err);
        message.err(err.response.content)
        projectServices.getProjects();
      });
  };

  return (
    <div className="flex justify-center items-center">
      <Form
        onFinish={onFinish}
        layout="vertical"
        style={{
          width: 800,
          maxWidth: 600,
        }}
      >
        <Form.Item
          name="projectName"
          label="Project name"
          rules={[
            {
              required: true,
              message: "Project name is required",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="projectCategory"
          label="Project category"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Select placeholder="Select a project category" allowClear="false">
            {projectCategory.map((item, index) => {
              return (
                <Option key={index} value={item.id}>
                  {item.projectCategoryName}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item name="description" label="Descriptions">
          <TextArea rows={16} />
        </Form.Item>

        <Form.Item>
          <Button className="bg-blue-600" type="primary" htmlType="submit">
            Create Project
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
