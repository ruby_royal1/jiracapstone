export const headerProject = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
  },
  {
    title: "PROJECT NAME",
    dataIndex: "projectName",
    width: 150,
    key: "projectName",
    render: (projectName) => <button className="text-blue-700" onClick={() => {console.log("test!!!!!")}}>{projectName}</button>,
  },
  {
    title: "CATEGORY NAME",
    dataIndex: "categoryName",
    key: "categoryName",
    width: 150,
  },
  {
    title: "CREATOR",
    dataIndex: "creator",
    render: (creator) => Object.values(creator),
    key: "creator",
    width: 150,
  },
  {
    title: "MEMBER",
    dataIndex: "members",
    render: (members) => members.map((member) => `${member.name}, `),
    key: "member",
    width: 150,
  },
  {
    title: "ACTION",
    dataIndex: "action",
    key: "action",
    width: 150,
  },
];
