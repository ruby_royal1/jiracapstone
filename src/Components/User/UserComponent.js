import React from "react";
import { useSelector } from "react-redux";
import { SettingOutlined } from "@ant-design/icons";
import { Button, Dropdown, Menu } from "antd";
import { localServices } from "../../service/localService/localLoginService";

export default function UserComponent() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });

  const items = [
    {
      label: "ATLASSIAN ADMIN",
      key: "0",
      disabled: true,
    },
    {
      label: <a href="/usersManagement">USER MANAGEMENT</a>,
      key: "1",
    },
    {
      label: "JIRA SETTINGS",
      key: "2",
      disabled: true,
    },
    {
      label: <a href="/projectsManagement">PROJECTS</a>,
      key: "3",
    },
  ];

  const userAction = (props) => {
    let userDropdownACtions = [];
    return (userDropdownACtions = [
      {
        label: (
          <h2 className="text-lg font-bold text-slate-500">Hello, {props}</h2>
        ),
        key: "0",
      },
      {
        label: "Edit Details",
        key: "1",
      },
      {
        label: (
          <Button className="bg-red-600 text-white" onClick={handleUserLogout}>
            Logout
          </Button>
        ),
        key: "2",
      },
    ]);
  };

  // console.log(userInfo);
  const handleUserLogout = () => {
    localServices.remove();
    window.location.href = "/loginPage";
  };
  let renderContent = () => {
    if (userInfo) {
      const menu1 = <Menu items={items} />;
      const menu2 = <Menu items={userAction(userInfo.name)} />;

      return (
        <div className="flex justify-around">
          <Dropdown className="mt-auto mx-auto" overlay={menu1} trigger={["click"]}>
            <a onClick={(e) => e.preventDefault()}>
              <SettingOutlined className="text-3xl" />
            </a>
          </Dropdown>

          <Dropdown className="m-3" overlay={menu2} trigger={["click"]}>
            <a onClick={(e) => e.preventDefault()}>
              <img
                style={{ width: 40, height: 35, borderRadius: "50%" }}
                src={userInfo.avatar}
              />
            </a>
          </Dropdown>
        </div>
      );
    }
  };

  return <>{renderContent()}</>;
}
