export const headerUser = [
  {
    title: "USER ID",
    dataIndex: "userId",
    key: "userId",
    width: 100,
  },
  {
    title: "USERNAME",
    dataIndex: "name",
    key: "name",
    width: 100,
  },
  {
    title: "EMAIL",
    dataIndex: "email",
    key: "email",
    width: 100,
  },
  {
    title: "PHONE NUMBER",
    dataIndex: "phoneNumber",
    key: "phoneNumber",
    width: 100,
  },
  {
    title: "ACTION",
    dataIndex: "action",
    key: "action",
    width: 100,
  },
];

