import React, { useEffect, useState } from "react";
import { Layout, Table, theme, Button } from "antd";
import { userService } from "../../service/userServices";
import { headerUser } from "./utility";

export default function UserManagementPage() {
  const { Content } = Layout;
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const [userList, setUserList] = useState([]);

  let fetchUserList = () => {
    userService
      .getUserList()
      .then((res) => {
        // console.log(res.data.content);
        let userArray = res.data.content.map((user) => {
          return {
            ...user,
            action: (
              <>
                <Button>Edit</Button>
                <Button>Delete</Button>
              </>
            ),
          };
        });
        setUserList(userArray);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    fetchUserList();
  }, []);

  return (
    <Layout>
      <Content
        className="site-layout"
        style={{
          padding: "0 50px",
        }}
      >
        <div
          className="container"
          style={{
            padding: 24,
            minHeight: 380,
            background: colorBgContainer,
          }}
        >
          <Table dataSource={userList} columns={headerUser} />
        </div>
      </Content>
    </Layout>
  );
}
