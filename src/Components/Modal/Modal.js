import React from "react";
import {
  Button,
  Cascader,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Radio,
  Select,
  Switch,
  TreeSelect,
} from "antd";
import { Multiselect } from "multiselect-react-dropdown";

export default function Modal({ open, onClose }) {
  const testArray = [
    { name: "thong", age: 28 },
    { name: "thinh", age: 28 },
    { name: "hoang", age: 28 },
  ];
  if (!open) {
    return null;
  }
  return (
    <div className="overlayyy">
      <div id="modalContainer">
        <div className="container">
          <div className="flex flex-col justify-center">
            <div className="flex justify-around">
              <h3>Create Task</h3>
              <h3 onClick={onClose}>X</h3>
            </div>
            <Form layout="vertical">
              <Form.Item label="Project">
                <Select>
                  <Select.Option value="demo">Demo</Select.Option>
                </Select>
              </Form.Item>
              <Form.Item
                name="taskName"
                label="Task name"
                rules={[
                  { required: true, message: "Please enter your task name" },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item label="Status">
                <Select>
                  <Select.Option value="test">Test</Select.Option>
                </Select>
              </Form.Item>

              <div className="flex justify-around">
                <Form.Item name="priority" label="Priority">
                  <Select>
                    <Select.Option value="test">Test</Select.Option>
                  </Select>
                </Form.Item>
                <Form.Item name="taskType" label="Task Type">
                  <Select>
                    <Select.Option value="test">Test</Select.Option>
                  </Select>
                </Form.Item>
              </div>

              <Form.Item name="assigners" label="Assigners">
                <Multiselect options={testArray} displayValue="name" />
              </Form.Item>

              <div className="flex justify-around">
                <h2>Time tracking</h2>
                <Form.Item
                  name="totalEstimatedHours"
                  label="Total Estimated Hours"
                >
                  <InputNumber />
                </Form.Item>
                <Form.Item name="hoursSpent" label="Hours spent">
                  <InputNumber />
                </Form.Item>
              </div>

              <Form.Item name="description" label="Description">
                <Input />
              </Form.Item>

              <Form.Item label="Button">
                <Button>Button</Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
