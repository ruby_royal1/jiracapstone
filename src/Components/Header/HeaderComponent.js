import React, { useState, useEffect } from "react";
import { Button, Layout, Menu } from "antd";
import { DownOutlined } from "@ant-design/icons";
import jiraLogo from "../../assets/images/Jira.png";
import UserComponent from "../User/UserComponent";
import { NavLink } from "react-router-dom";


export default function HeaderComponent({ setOpen }) {
  const [current, setCurrent] = useState("Project");

  const onClick = (e) => {
    setCurrent(e.key);
  };


  const { Header } = Layout;
  const items = [
    {
      label: <span className="hover:text-black">Project</span>,
      key: "Project",
      children: [
        {
          type: "group",
          children: [
            {
              label: (
                <NavLink to="/projectsManagement">View all projects</NavLink>
              ),
              key: "setting:1",
            },
            {
              label: (
                <NavLink to="/createProject">
                  <button className="hover:text-black hover:font-semibold">
                    Create project
                  </button>
                </NavLink>
              ),
              key: "setting:2",
            },
          ],
        },
      ],
      icon: <DownOutlined />,
    },
    {
      label: "User",
      key: "User",
      children: [
        {
          type: "group",
          children: [
            {
              label: <NavLink to="/usersManagement">View all users</NavLink>,
              key: "setting:3",
            },
          ],
        },
      ],
      icon: <DownOutlined />,
    },
    {
      label: (
        <Button
          className="border-none"
          onClick={() => {
            setOpen(true);
          }}
        >
          Create Task
        </Button>
      ),
      key: "CreateTask",
    },
  ];

  return (
    <div>
      <Header
        style={{
          background: "#fff",
          position: "sticky",
          top: 0,
          width: "100%",
        }}
      >
        <NavLink to="/projectsManagement">
          <div
            className="hover:bg-slate-300 hover:rounded"
            style={{
              zIndex: 1,
              float: "left",
              width: "60px",
              height: "100%",
              backgroundImage: `url("${jiraLogo}")`,
              backgroundPosition: "Center",
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
            }}
          />
        </NavLink>
        <div className="flex justify-around">
          <Menu
            className="text-blue-500"
            style={{ background: "#fff", width: "100%" }}
            onClick={onClick}
            selectedKeys={[current]}
            mode="horizontal"
            items={items}
          />
          <UserComponent />
        </div>
      </Header>
    </div>
  );
}
