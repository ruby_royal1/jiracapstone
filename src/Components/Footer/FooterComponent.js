import React from "react";
import { Layout } from "antd";
const { Footer } = Layout;

export default function FooterComponent() {
  return (
    <Footer
      style={{
        textAlign: "center",
        width: "100%",
      }}
    >
      Ant Design ©2023 Created by Ant UED ~ Cloned Jira
    </Footer>
  );
}
