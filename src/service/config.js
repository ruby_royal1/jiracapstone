export const BASE_URL = "https://jiranew.cybersoft.edu.vn";

let auth =
  "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9lbWFpbGFkZHJlc3MiOiJ0aG9uZ0BnbWFpbC52biIsIm5iZiI6MTY4Mjc0Mzc2NiwiZXhwIjoxNjgyNzQ3MzY2fQ.5OLcBSGhsRv6BA8mksAccCGX1BcYduvtEhHULfQ7Duw";
const token =
  "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9lbWFpbGFkZHJlc3MiOiJzdHJpbmciLCJuYmYiOjE2MjcyOTAzODYsImV4cCI6MTYyNzI5Mzk4Nn0.75DGRowsyI7Sl6bmYgKuZ8oaG36fOr0TUWbUwAjtDs";

export const headersConfig = () => {
  return {
    Authorization: auth,
    TokenCybersoft: token,
  };
};
