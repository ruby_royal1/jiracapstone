import axios from "axios";
import { BASE_URL, headersConfig } from "./config";

export const userService = {
  postLogin: (userInput) => {
    return axios({
      url: `${BASE_URL}/api/Users/signin`,
      method: "POST",
      data: userInput,
      headers: headersConfig(),
    });
  },

  getUserList: () => {
    return axios({
      url: `${BASE_URL}/api/Users/getUser`,
      method: "GET",
      headers: headersConfig(),
    });
  },

  userRegister: (userInput) => {
    return axios({
      url: `${BASE_URL}/api/Users/signup`,
      method: "POST",
      headers: headersConfig(),
      data: userInput,
    });
  },
};
