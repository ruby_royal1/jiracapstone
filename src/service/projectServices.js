import axios from "axios";
import { BASE_URL, headersConfig } from "./config";

const projectServices = {
  getProjects: () => {
    return axios({
      url: `${BASE_URL}/api/Project/getAllProject`,
      method: "GET",
      headers: headersConfig(),
    });
  },

  deleteProject: (projectID) => {
    return axios({
      url: `${BASE_URL}/api/Project/deleteProject?projectId=${projectID}`,
      method: "DELETE",
      headers: headersConfig(),
    });
  },

  getAllProjectCategory: () => {
    return axios({
      url: `${BASE_URL}/api/ProjectCategory`,
      method: "GET",
      headers: headersConfig(),
    });
  },

  createProject: (data) => {
    return axios({
      url: `${BASE_URL}/api/Project/createProjectAuthorize`,
      method: "POST",
      headers: headersConfig(),
      data: data,
    });
  },
};

export default projectServices;
