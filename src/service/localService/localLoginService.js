export const localServices = {
  set: (severRes) => {
    let userInformation = JSON.stringify(severRes);
    localStorage.setItem("userInformationJSON", userInformation);
  },

  get: () => {
    const USER_JSON = localStorage.getItem("userInformationJSON");
    return JSON.parse(USER_JSON);
  },

  remove: () => {
    localStorage.removeItem("userInformationJSON");
  },
};
